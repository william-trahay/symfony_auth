## Symfony Auth

- symfony new symfony_auth --version="5.4.*" --webapp
- php bin/console doctrine:database:create
- php bin/console make:user 
- php bin/console make:migration
- php bin/console doctrine:migrations:migrate
- php bin/console make:auth
- php bin/console make:registration-form